Requisitos: 
Python 3.8.4 ou superior. https://www.python.org/downloads/
Modulos requirements.txt instalados automaticamente. Em caso de falha, tentar manual.

1) Salvar na pasta "Folhas", todas as folhas de ponto com a seguinte nomenclatura:

diasNoMes_ultimoDiaUtil_Mes_Ano.pdf

Exemplos na pasta Folhas.

2) Alterar os modelos para imagens com fundo transparente e ALTURA de 25px para os DÍGITOS e Dois pontos. 
Recomendável largura entre 18 e 25.

3) No código, ajustar para a sua assinatura:
Comentário: #px=px + 1120 -> 1120 é para a minha rubrica, ajustar para o tamanho da sua assinatura

4) Uso:
py preencheFolhaPonto.py

