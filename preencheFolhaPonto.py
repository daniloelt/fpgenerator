from PIL import Image
from pdf2image import convert_from_path
import os
from os import listdir
import datetime as dt
import random

class Preenche:
    def __init__(self):
        super(Preenche, self).__init__()

    def run(self, nome_folha):
        base_path = os.getcwd() + '\\'
        pages = convert_from_path(poppler_path = base_path + '\\poppler-0.68.0\\bin', pdf_path = base_path + 'Folhas\\' + nome_folha, dpi = 500)
        for page in pages:
            page.save(nome_folha.replace('.pdf', '') +'.jpg', 'JPEG')

        params_mes = nome_folha.replace('.pdf', '').split('_')
        prox_dia = 47
        numero_dias = int(params_mes[0])
        ano = params_mes[3]
        ultimo_dia_util = params_mes[1] + '/' + params_mes[2] + '/' + ano[2] + ano[3]
        print(ultimo_dia_util)
        im = Image.open(base_path + nome_folha.replace('.pdf', '') +'.jpg').resize((1700, 2200)) 
        px = 260
        py = 438
        contador = False
        rgb_im = im.convert('RGB')
        
        #px=px + 780 -> 780 é para a minha rubrica, ajustar para o tamanho da sua assinatura
        self._horarioToImage(nome_folha= nome_folha.replace('.pdf', ''), img=im, hora='assinatura', px=px + 780, py=(1995+47*(numero_dias%30)), var_horizontal=True, variacao=30)
        self._horarioToImage(nome_folha= nome_folha.replace('.pdf', ''), img=im, hora=ultimo_dia_util, px=px-113, py=(1993+47*(numero_dias%30)), var_horizontal=True, variacao=30)

        for x in range(438, 438+prox_dia*numero_dias, prox_dia):
            r, g, b = rgb_im.getpixel((px, x))
            entrada = self.sortearHorario(dt.datetime.strptime('9:00', '%H:%M'), 8)
            interv_start = self.sortearHorario(dt.datetime.strptime('12:00', '%H:%M'), 4)
            interv_end = self.sortearHorario(dt.datetime.strptime('14:00', '%H:%M'), 4)
            fim = self.sortearHorario(dt.datetime.strptime('19:00', '%H:%M'), 8)
            if(r > 240):
                if contador:
                    py -= 3

                self._horarioToImage(nome_folha= nome_folha.replace('.pdf', ''),img=im, hora=entrada, px=px, py=py, var_horizontal=True, variacao=12)
                self._horarioToImage(nome_folha= nome_folha.replace('.pdf', ''),img=im, hora=interv_start, px=px+160, py=py, var_horizontal=False, variacao= 0)
                self._horarioToImage(nome_folha= nome_folha.replace('.pdf', ''),img=im, hora=interv_end, px=px+270, py=py, var_horizontal=False, variacao = 0)
                self._horarioToImage(nome_folha= nome_folha.replace('.pdf', ''),img=im, hora=fim, px=px+420, py=py, var_horizontal=True, variacao=12)
                #px=px + 1120 -> 1120 é para a minha rubrica, ajustar para o tamanho da sua assinatura
                self._horarioToImage(nome_folha= nome_folha.replace('.pdf', ''), img=im, hora='assinatura', px=px + 1120, py=py, var_horizontal=True, variacao=20)
                contador = False
            else:
                contador = True

            py += prox_dia
        im.save(base_path + nome_folha)

    def sortearHorario(self, horario_base: dt, range: int):
        r = (-1)**random.randrange(2) * random.randrange(range)
        data = horario_base + dt.timedelta(minutes=r)
        return data.strftime('%H:%M')

    def _horarioToImage(self, nome_folha: str, img: Image, hora: str, px: int, py: int, var_horizontal: bool, variacao: int):
        base_path = os.getcwd() + '\\'
        modules = 'modelos\\'
        if var_horizontal:
            px += random.randrange(variacao)

        if hora == 'assinatura':
            imgN = Image.open(base_path + modules + 'assinatura.jpg')
            img.paste(imgN, (px, py))
            img.save(base_path + nome_folha + '.jpg')
            return


        if (hora[0] == '0'):
            hora = hora[1:]

        s = len(hora)

        for x in range(0,s):
            if hora[x] == '/':
                px += 20
            elif (hora[x] == ':'):
                imgN = Image.open(base_path + modules + 'doispontos.jpg')
                img.paste(imgN, (px, py))
                px += 18
            elif (hora[x] == '0'):
                imgN = Image.open(base_path + modules + hora[x] + '.jpg')
                img.paste(imgN, (px, py))
                px += 22
            else:
                imgN = Image.open(base_path + modules + hora[x] + '.jpg')
                img.paste(imgN, (px, py))
                px += 20

        img.save(base_path + nome_folha + '.jpg')


if __name__=='__main__':
    os.system('py -m pip install -r requirements.txt')
    p = Preenche()
    folhas = listdir(os.getcwd() + '\\Folhas')
    for f in folhas:
        p.run(nome_folha=f)